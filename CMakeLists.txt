set(CMAKE_C_COMPILER "/Library/Developer/CommandLineTools/usr/bin/cc")
set(CMAKE_CXX_COMPILER "/Library/Developer/CommandLineTools/usr/bin/c++")

project(ExtendedKF)

cmake_minimum_required (VERSION 3.5)

add_definitions(-std=c++0x)

set(sources
    src/FusionEKF.cpp
    src/kalman_filter.cpp
    src/main.cpp
    src/tools.cpp)

add_executable(ExtendedKF ${sources})
